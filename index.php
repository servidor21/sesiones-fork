<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sesiones y cookiee</title>
    </head>
    <body>
        <h1>Ejercicios sobre cookies, sesiones y autenticación</h1>
        <p><a href="privada">Privado:</a> acceso controlado por Apache</p>                
        <p><a href="privada2">Privado 2:</a> php recoge los nombres y hace la autenticación. Aquí está sin hacer.</p>                
        <p><a href="privada3">Privada 3.</a>  Con validación contra BBDD y recuerdo del visitante</p>   
        <p><a href="contador1.php">Contador con cookies</a></p>   
        <p><a href="contador2.php">Contador con sesiones</a></p>           
    </body>
</html>